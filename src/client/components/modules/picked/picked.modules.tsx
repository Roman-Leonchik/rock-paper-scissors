import React, { useState, useEffect } from "react";
import { ButtonCircle, IButtons } from "../../elements/button-circle/button-circle.elements";
import {
    StyledPickedWrapp,
    StyledPickedText,
    StyledPickedItem,
} from "./picked.styled";

interface IProps extends IButtons {}

export const Picked: React.FC<IProps> = ({ onClick, type }) => {
    const [counter, setCounter] = useState(3);

    useEffect(() => {
        if(counter > 0) {
            setTimeout(() => {setCounter(counter-1)}, 1000)
        }
    }, [counter]);

    return(
        <StyledPickedWrapp>
            <StyledPickedItem>
                <StyledPickedText>You picked</StyledPickedText>
                <ButtonCircle onClick={onClick} type={type} picked={true}/>
            </StyledPickedItem>
            <div></div>
            <StyledPickedItem>
                <StyledPickedText>The bot picked</StyledPickedText>
                <div>
                    {counter}
                </div>
            </StyledPickedItem>
        </StyledPickedWrapp>
    )
}