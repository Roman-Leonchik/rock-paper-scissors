import styled from "styled-components";

export const StyledPickedWrapp = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: center;
    max-width: 1000px;
    margin: 0 auto;
    text-align: center;
`;

export const StyledPickedText = styled.p`
    font-size: 24px;
    margin: 0 auto 60px;
    text-transform: uppercase;
    font-weight: 600;
    letter-spacing: 3px;
`;

export const StyledPickedItem = styled.div`
    padding: 0 15px;
    min-width: 380px;
`;
