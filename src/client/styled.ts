import styled from "styled-components";

export const StyledPage = styled.div`
    padding: 45px 0 20px;
    min-height: 100vh;
    position: relative;
    color: ${({theme}) => theme.white};
    background: linear-gradient(${({theme}) => theme.bgPage});
`;

export const StyledContainer = styled.div`
    padding: 0 15px;
    margin: 0 auto;
    max-width: 1200px;
`;

export const StyledLogo= styled.img`
    padding-top: 5px;
`;

export const StyledScoreWrapp = styled.div`
    max-width: 700px;
    margin: 0 auto 70px;
    padding: 20px 20px 20px 30px;
    border: 3px solid ${({theme}) => theme.рeaderOutline};
    border-radius: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-start;
`;

export const StyledScore = styled.div`
    background-color: ${({theme}) => theme.white};
    padding: 15px 10px;
    border-radius: 10px;
    min-width: 150px;
    text-align: center;
`;

export const StyledScoreText = styled.div`
    color: ${({theme}) => theme.scoreText};
    font-size: 16px;
    text-transform: uppercase;
    font-weight: 600;
    letter-spacing: 2.5px;
`;

export const StyledScoreNumber = styled.div`
    font-weight: 700;
    color: ${({theme}) => theme.darkText};
    font-size: 62px;
    line-height: 1;
`;

export const StyledButtonChoice = styled.div`
    display: flex;
    justify-content: center;
    max-width: 500px;
    margin: 0 auto;
    flex-wrap: wrap;
    gap: 25px 80px;
    justify-content: center;
    background-image: url(${require("@assets/img/bg-triangle.svg")});
    background-position: 50% 62%;
    background-repeat: no-repeat;
`;
