import React, { useState } from "react";
import {
    StyledPage,
    StyledContainer,
    StyledScoreWrapp,
    StyledScore,
    StyledScoreText,
    StyledScoreNumber,
    StyledButtonChoice,
    StyledLogo,
} from "./styled";
import CompositeProvider from "./providers/composite/composite.providers";
import { Picked } from "./components/modules/picked/picked.modules";
import { ButtonCircle } from "./components/elements/button-circle/button-circle.elements";

export const App = () => {
    const [score, setScore] = useState("12");
    const [choice, setChoice] = useState(true);
    const [picked, setPicked] = useState("");

    const listButtonStandart = ["paper", "scissors", "rock"];

    const hadlerClick = (type: string) => {
        setPicked(type);
        setChoice(false);
    };

    return (
        <CompositeProvider>
            <StyledPage>
                <StyledContainer>
                    <StyledScoreWrapp>
                        <StyledLogo src={require(`@assets/img/logo.svg`)} alt="logo" />
                        <StyledScore>
                            <StyledScoreText>score</StyledScoreText>
                            <StyledScoreNumber>{score}</StyledScoreNumber>
                        </StyledScore>
                    </StyledScoreWrapp>
                    {
                        choice
                            ?
                                <StyledButtonChoice>
                                    {listButtonStandart.map((item) => <ButtonCircle onClick={hadlerClick} type={item}/>)}
                                </StyledButtonChoice>
                            :
                                <Picked onClick={hadlerClick} type={picked}/>
                    }
                </StyledContainer>
            </StyledPage>
        </CompositeProvider>
    );
};
